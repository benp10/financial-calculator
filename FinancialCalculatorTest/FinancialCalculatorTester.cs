﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Finance.Test
{
    [TestFixture]
    public class FinancialCalculatorTester
    {
        [Test]
        public void TestPresentValueByFutureValue()
        {
            double pv = FinancialCalculator.PresentValue(futureValue: 1000, rate: 0.03, nofPeriods: 10);
            AssertDoubleValue(744.0939, pv);

            pv = FinancialCalculator.PresentValue(futureValue: 1000, rate: 0.03, nofPeriods: 1);
            AssertDoubleValue(970.8737, pv);

            pv = FinancialCalculator.PresentValue(futureValue: 1000, rate: 0.03, nofPeriods: 0);
            Assert.AreEqual(1000, pv);

            pv = FinancialCalculator.PresentValue(futureValue: 1000, rate: 0, nofPeriods: 10);
            Assert.AreEqual(1000, pv);
        }

        [Test]
        public void TestPresentValueByPayment()
        {
            double pv = FinancialCalculator.PresentValue(rate: 0.07, nofPeriods: 10, payment: 100);
            AssertDoubleValue(702.3581, pv);

            pv = FinancialCalculator.PresentValue(rate: 0.07, nofPeriods: 100, payment: 1);
            AssertDoubleValue(14.2692, pv);

            pv = FinancialCalculator.PresentValue(rate: 0.07, nofPeriods: 10, payment: 100, timing: FinancialCalculator.PaymentTimimg.Begin);
            AssertDoubleValue(751.5232, pv);

            pv = FinancialCalculator.PresentValue(rate: 0.07, nofPeriods: 100, payment: 1, timing: FinancialCalculator.PaymentTimimg.Begin);
            AssertDoubleValue(15.268, pv);

            pv = FinancialCalculator.PresentValue(rate: 0, nofPeriods: 10, payment: 25000);
            Assert.AreEqual(250000, pv);
        }

        [Test]
        public void TestPresentValueByCashFlow()
        {
            double pv = FinancialCalculator.PresentValue(0.05, 100, 200, 300, 2000, 550);
            AssertDoubleValue(2612.1396, pv);

            pv = FinancialCalculator.PresentValue(0.05, 1, 2, 7, 20, -5, 10, 12);
            AssertDoubleValue(37.34, pv);

            pv = FinancialCalculator.PresentValue(0.05, FinancialCalculator.PaymentTimimg.Begin, 100, 200, 300, 2000, 550);
            AssertDoubleValue(2742.7466, pv);

            pv = FinancialCalculator.PresentValue(0.05, FinancialCalculator.PaymentTimimg.Begin, 1, 2, 7, 20, -5, 10, 12);
            AssertDoubleValue(39.207, pv);

            pv = FinancialCalculator.PresentValue(0, 1, 2, 7, 20, -5, 10, 12);
            Assert.AreEqual(47, pv);
        }

        [Test]
        public void TestFutureValue()
        {
            double fv = FinancialCalculator.FutureValue(rate: 0.025, nofPeriods: 10, payment: 12.5);
            AssertDoubleValue(140.0422, fv);

            fv = FinancialCalculator.FutureValue(rate: 0.025, nofPeriods: 10, payment: 12.5, presentValue: 100);
            AssertDoubleValue(268.0507, fv);

            fv = FinancialCalculator.FutureValue(rate: 0.025, nofPeriods: 10, payment: 12.5, presentValue: 100, growthRate: 0.01);
            // TOOO - Assert

            fv = FinancialCalculator.FutureValue(rate: 0.025, nofPeriods: 10, payment: 12.5, presentValue: 100, growthRate: 0.01, timing: FinancialCalculator.PaymentTimimg.Begin);
            // TODO - Assert

            fv = FinancialCalculator.FutureValue(rate: 0.1, nofPeriods: 30, payment: 0, presentValue: 1000);
            AssertDoubleValue(17449.4023, fv);

            fv = FinancialCalculator.FutureValue(rate: 0.1, nofPeriods: 30, payment: 0, presentValue: 1000, growthRate: 0, timing: FinancialCalculator.PaymentTimimg.Begin);
            AssertDoubleValue(17449.4023, fv);

            fv = FinancialCalculator.FutureValue(rate: 0, nofPeriods: 10, payment: 100, presentValue: 100);
            Assert.AreEqual(1100, fv);
        }

        [Test]
        public void TestPayment()
        {
            double pmt = FinancialCalculator.Payment(rate: 0.035, nofPeriods: 12, presentValue: 1500);
            AssertDoubleValue(155.226, pmt);

            pmt = FinancialCalculator.Payment(rate: 0.035, nofPeriods: 12, presentValue: 1500, timing: FinancialCalculator.PaymentTimimg.Begin);
            AssertDoubleValue(149.9767, pmt);

            pmt = FinancialCalculator.Payment(rate: 0.05, nofPeriods: 100, presentValue: 50000);
            AssertDoubleValue(2519.157, pmt);

            pmt = FinancialCalculator.Payment(rate: 0.05, nofPeriods: 1, presentValue: 2000);
            AssertDoubleValue(2100, pmt);

            pmt = FinancialCalculator.Payment(rate: 0.05, nofPeriods: 1, presentValue: 2000, timing: FinancialCalculator.PaymentTimimg.Begin);
            AssertDoubleValue(2000, pmt);

            pmt = FinancialCalculator.Payment(rate: 0, nofPeriods: 5, presentValue: 2000);
            AssertDoubleValue(400, pmt);
        }

        [Test]
        public void TestInternalRateOfReturn()
        {
            double irr = FinancialCalculator.InternalRateOfReturn(investment: 1000, payment: 100, nofPeriods: 12);
            AssertDoubleValue(0.02923, irr);
        }

        /// <summary>
        /// Asserts whether the expected and actual values are equal in precision of 4 digits
        /// </summary>
        private void AssertDoubleValue(double expected, double actual)
        {
            Assert.LessOrEqual(Math.Abs(actual - expected), 0.0001);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finance
{
    public class FinancialCalculator
    {
        /// <summary>
        /// This enum can be used to determine whether payments are recieved or payed in the
        /// begining or end of each period. This affects the return values. Default value is End.
        /// </summary>
        public enum PaymentTimimg { Begin = 1, End = 2 }

        /// <summary>
        /// Returns the present value of a given future value
        /// </summary>
        /// <param name="FutureValue">The future value to discount</param>
        /// <param name="rate">The interest rate that will be used for discounting the future value. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        public static double PresentValue(double futureValue, double rate, int nofPeriods)
        {
            double pv = futureValue / Math.Pow(1 + rate, nofPeriods);
            return pv;
        }

        /// <summary>
        /// Returns the present value based on the formula:
        /// PV = PMT/R * [1 - 1/(1+R)^n] * (1+R)
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        public static double PresentValue(double rate, int nofPeriods, double payment)
        {
            return PresentValue(rate, nofPeriods, payment, PaymentTimimg.End);
        }

        /// <summary>
        /// Returns the present value based on the formula:
        /// PV = PMT/R * [1 - 1/(1+R)^n] * (1+R)
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        /// <param name="timing">
        /// Determines whether payments are received in the beginning or end of each time period
        /// </param>
        public static double PresentValue(double rate, int nofPeriods, double payment, PaymentTimimg timing)
        {
            if (rate == 0) return nofPeriods * payment;

            double pv = payment / rate * (1 - 1 / Math.Pow(1 + rate, nofPeriods));
            pv = pv * (timing == PaymentTimimg.Begin ? (1 + rate) : 1);
            return pv;
        }

        /// <summary>
        /// Returns the present value of the given cash flow based on the given interest rate
        /// </summary>
        /// <param name="rate">The rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="cashFlow">Cash flow list, starting from the cash flow of the 1st period</param>
        public static double PresentValue(double rate, params double[] cashFlow)
        {
            return PresentValue(rate, PaymentTimimg.End, cashFlow);
        }

        /// <summary>
        /// Returns the present value of the given cash flow based on the given interest rate
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="timing">Determines whether payments are received in the beginning or end of each time period</param>
        /// <param name="cashFlow">Cash flow list, starting from the cash flow of the 1st period</param>
        public static double PresentValue(double rate, PaymentTimimg timing, params double[] cashFlow)
        {
            double pv = 0;
            for (int index = 0; index < cashFlow.Length; index++)
            {
                pv += cashFlow[index] / Math.Pow(1 + rate, index + 1);
            }

            pv = pv * (timing == PaymentTimimg.Begin ? (1 + rate) : 1);
            return pv;
        }

        /// <summary>
        /// Returns the calculated future value based on the following formula
        /// FV = PMT/R * (1+R) * [(1+R)^n - 1]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        public static double FutureValue(double rate, int nofPeriods, double payment)
        {
            return FutureValue(rate, nofPeriods, payment, 0);
        }

        /// <summary>
        /// Returns the calculated future value based on the following formula
        /// FV = PMT/R * (1+R) * [(1+R)^n - 1]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        /// <param name="presentValue">The present value of the financial asset</param>
        public static double FutureValue(double rate, int nofPeriods, double payment, double presentValue)
        {
            return FutureValue(rate, nofPeriods, payment, presentValue, 0, PaymentTimimg.End);
        }

        /// <summary>
        /// The above formula can be extended:
        /// FV = [PMT*(1+g)^(n-1)] /R * (1+R) * [(1+R)^n - 1]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the present value and cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        /// <param name="presentValue">The present value of the financial asset</param>
        /// <param name="growthRate">The rate in which the the payment is growing every period. Use 0.01 for 1% rate.</param>
        public static double FutureValue(double rate, int nofPeriods, double payment, double presentValue, double growthRate)
        {
            return FutureValue(rate, nofPeriods, payment, presentValue, growthRate, PaymentTimimg.End);
        }

        /// <summary>
        /// The above formula can be extended:
        /// FV = [PMT*(1+g)^(n-1)] /R * (1+R) * [(1+R)^n - 1]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the present value and cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="payment">The periodical cash flow</param>
        /// <param name="presentValue">The present value of the financial asset</param>
        /// <param name="growthRate">The rate in which the the payment is growing every period. Use 0.01 for 1% rate.</param>
        /// <param name="timing">Determines whether payments are received in the beginning or end of each time period</param>
        public static double FutureValue(double rate, int nofPeriods, double payment, double presentValue, double growthRate, PaymentTimimg timing)
        {
            double paymentFV = 0;
            if (rate != 0)
                paymentFV = (payment * Math.Pow(1 + growthRate, nofPeriods - 1)) / rate * (Math.Pow(1 + rate, nofPeriods) - 1);
            else
                paymentFV = payment * nofPeriods; // for now I ignore growthRate in that case - TODO

            paymentFV = paymentFV * (timing == PaymentTimimg.Begin ? (1 + rate) : 1);
            double presentvalueFv = presentValue * Math.Pow(1 + rate, nofPeriods);
            return presentvalueFv + paymentFV;
        }

        /// <summary>
        /// Returns the periodical cash flow based on the following formula:
        /// PV = PMT/R * [1 - 1/(1+R)^n]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the present value and cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="presentValue">The present value of the financial asset</param>
        public static double Payment(double rate, int nofPeriods, double presentValue)
        {
            return Payment(rate, nofPeriods, presentValue, PaymentTimimg.End);
        }

        /// <summary>
        /// Returns the periodical cash flow based on the following formula:
        /// PV = PMT/R * [1 - 1/(1+R)^n]
        /// </summary>
        /// <param name="rate">The interest rate that will be used for discounting the present value and cash flow. Use 0.05 for 5% rate.</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="presentValue">The present value of the financial asset</param>
        /// <param name="timing">Determines whether payments are received in the beginning or end of each time period</param>
        public static double Payment(double rate, int nofPeriods, double presentValue, PaymentTimimg timing)
        {
            if (rate == 0) return presentValue / nofPeriods;

            double pmt = presentValue * rate / (1 - 1 / Math.Pow(1 + rate, nofPeriods));
            pmt = pmt / (timing == PaymentTimimg.Begin ? (1 + rate) : 1);
            return pmt;
        }

        /// <summary>
        /// Returns the internal rate of return (IRR) of the given investment and cash flow.
        /// </summary>
        /// <param name="investment">The investment made at present time</param>
        /// <param name="payment">The periodical payment</param>
        /// <param name="nofPeriods">The number of periods</param>
        public static double InternalRateOfReturn(double investment, double payment, int nofPeriods)
        {
            return InternalRateOfReturn(investment, payment, nofPeriods, PaymentTimimg.End);
        }

        /// <summary>
        /// Returns the internal rate of return (IRR) of the given investment and cash flow.
        /// </summary>
        /// <param name="investment">The investment made at present time</param>
        /// <param name="payment">The periodical payment</param>
        /// <param name="nofPeriods">The number of periods</param>
        /// <param name="timing">Determines whether payments are received in the beginning or end of each time period</param>
        public static double InternalRateOfReturn(double investment, double payment, int nofPeriods, PaymentTimimg timing)
        {
            double max = 0;
            double min = 0;
            double rate = 0.1; // Initial guess (10%)
            double pv = PresentValue(rate, nofPeriods, payment, timing);
            while (Math.Abs(pv - investment) > 0.05)
            {
                // Divide and conquer
                if (pv > investment)
                {
                    max = rate;
                    rate = (rate + min) / 2;
                }
                else
                {
                    min = rate;
                    rate = (rate + max) / 2;
                }

                // Check again
                pv = PresentValue(rate, nofPeriods, payment, timing);
            }

            return rate;
        }
    }
}
